Proceso sumaN
	definir suma,n, i como entero;
	
	suma<-0;
	n<-0;
	Para i<-0 Hasta 9 Con Paso 1 Hacer
		n<-Aleatorio(0,10);
		suma<-suma+n;
		Escribir Sin Saltar n," + ";
	FinPara
	Escribir " ";
	Escribir suma;
	
FinProceso
