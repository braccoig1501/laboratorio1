Proceso sumaValores
	
	Definir i, n1,n2, aux, acum Como Entero;
	
	Escribir "Escribir el primer numero";
	Leer n1;	
	Escribir "Escribir el segundo numero";
	leer n2;
	
	aux<-0;
	
	Para i<-n1+1 Hasta n2-1 Con Paso 1 Hacer
		aux<-aux+i;
	FinPara
	
	Escribir "La sumatoria es: ", aux;
	
FinProceso
