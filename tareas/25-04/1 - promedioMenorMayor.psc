

Proceso promedioMenorMayor
	//Realizar un programa que reciba 5 numeros
	// Muestre el promedio, el menor y el mayor
	Definir i, arreglo, cantidad como entero;
	Dimension arreglo[100];
	
	Escribir "Ingrese la cantidad de numeros a ingresar: ";
	leer cantidad;
	
	solicitarNumeros(cantidad);	
	
	escribir "El promedio es: ", calcularPromedio(arreglo,cantidad);	
	escribir "El mayor es: ", guardarMayor(arreglo,cantidad);
	escribir "El menor es: ", guardarMenor(arreglo,cantidad);	
		
	
FinProceso

SubProceso solicitarNumeros (n) 
	Definir i, arreglo Como Entero;
	Dimension arreglo[100];
	
	escribir "Ingrese los numeros: ";
    Para i<-0 Hasta n-1 Con Paso 1 Hacer
		leer arreglo[i];
	FinPara
	
FinSubProceso

// funcion que recibe un argumento por valor, y devuelve su doble
SubProceso promedio <- calcularPromedio (num,n) 
    Definir promedio,acum como real;
	Definir i Como Entero;
	acum<-0;
	promedio<-0;
    Para i<-0 Hasta n-1 Con Paso 1 Hacer
		acum<-acum+num[i];
	FinPara
	promedio<-acum/5;
FinSubProceso


SubProceso mayor <- guardarMayor (num,n) 
	Definir i,auxMayor,mayor Como Entero;
	mayor<-num[1];
    Para i<-1 Hasta n-1 Con Paso 1 Hacer
		Si num[i]>mayor Entonces
			mayor<-num[i];
		FinSi
	FinPara
FinSubProceso


SubProceso menor <- guardarMenor (num,n) 
	Definir i,auxMenor,menor Como Entero;
	menor<-0;
    Para i<-0 Hasta n-1 Con Paso 1 Hacer
		Si num[i]<menor | menor = 0  Entonces
			menor<-num[i];
		FinSi
	FinPara
FinSubProceso