Proceso cuentaNumerosRandom
	
	Definir _numero, contPos, contNeg,contM,contZero,i como entero;
	contM<-0;
	contPos<-0;
	contNeg<-0;
	contZero<-0;
	Dimension _numero[20];
	Escribir "Generando numeros aleatoreos del -10 al 10: ";
	Repetir		
		_numero[contM]<-Aleatorio(-10,10);
		Escribir contM+1," )", _numero[contM]; 
		Si _numero[contM]>0 Entonces
			contPos<-contPos+1;
		FinSi
		Si _numero[contM]<0 Entonces
			contNeg<-contNeg+1;
		FinSi
		Si _numero[contM]=0 Entonces
			contZero<-contZero+1;
		FinSi
		contM<-contM+1;
	Hasta Que contM=20
	
	Escribir Sin Saltar "Positivos: ";
	Para i<-1 Hasta contPos Con Paso 1 Hacer
		Escribir Sin Saltar " * ";
	FinPara
	
	
	Escribir " ";
	
	Escribir Sin Saltar "Negativos: ";
	
	Para i<-1 Hasta contNeg Con Paso 1 Hacer
		Escribir Sin Saltar " * ";
	FinPara
	Escribir " ";
	
	Escribir Sin Saltar "Ceros: ";
	Para i<-1 Hasta contZero Con Paso 1 Hacer
		Escribir Sin Saltar " * ";
	FinPara
	Escribir " ";
	
	
FinProceso
