Proceso valoresRandom
	Definir _numeros, i Como Entero;
	
	Dimension _numeros[20];
	
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		_numeros[i]<-Aleatorio(-10,10);
		Escribir "Pos: ", i+1, " Numero: ", _numeros[i];
	FinPara
	
FinProceso
