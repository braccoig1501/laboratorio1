Proceso contadorDeNumeros
	
	Definir _numero, contPos, contNeg,i como entero;
	
	contPos<-0;
	contNeg<-0;
	Repetir
		Escribir "Ingrese numeros y termine de ingresar con 0";
		leer _numero;
		Si _numero>0 Entonces
			contPos<-contPos+1;
		FinSi
		Si _numero<0 Entonces
			contNeg<-contNeg+1;
		FinSi
	Hasta Que _numero=0
	
	Escribir Sin Saltar "Positivos: ";
	Para i<-1 Hasta contPos Con Paso 1 Hacer
		Escribir Sin Saltar " * ";
	FinPara
	
	
	Escribir " ";
	
	Escribir Sin Saltar "Negativos: ";
	
	Para i<-1 Hasta contNeg Con Paso 1 Hacer
		Escribir Sin Saltar " * ";
	FinPara
	Escribir " ";
	
	
FinProceso
