Proceso multiplosDeTres
	
		Definir _numero, contador, limite, numeroMultiplos Como Entero;
		
		Escribir "Ingresa el n�mero de multiplos a calcular: ";
		Leer numeroMultiplos;
		
		Escribir "Ingresa el valor de numero para calcular sus ", numeroMultiplos ," multiplos:";
		Leer _numero;
		
		limite <- 1;
		contador <- 1;
		
		Repetir
			Si contador%_numero = 0 Entonces
				Escribir limite, ") ", contador;
				limite <- limite + 1;
			FinSi			
			contador <- contador + 1;			
		Hasta Que limite > numeroMultiplos

FinProceso
