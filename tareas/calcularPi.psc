Proceso calcularValorPi	
	Definir i,n,aux Como Entero;
	Definir auxPar,auxImpar,result Como Real;
	Escribir "Ingrese valor de n: ";
	leer n;
	auxPar <- 0;
	auxImpar <- 0;
	aux <- 1;	
	Para i<- 1 Hasta n Con Paso 1 Hacer	
		Si (i % 2) = 0 Entonces			
			auxImpar <- auxImpar + (1/aux);
		SiNo
			auxPar <- auxPar + (1/aux);			
		FinSi
		aux <- aux+2;		
	FinPara
	
	result <- 4*(auxPar-auxImpar);	
	Escribir "El valor aproximado de pi es: ", result;	
FinProceso
