Proceso riegosEnfermedadCoronaria
	Definir peso, altura, IMC Como Real;
	Definir edad Como Entero;
	Escribir 'Ingrese su peso: ';	
	Leer peso;
	Escribir 'Ingrese su altura: ';
	Leer altura;
	Escribir 'Ingrese su edad: ';
	Leer edad;
	
	IMC <- peso/(altura^2);
	
	Si (IMC < 22.0) & (edad < 45) Entonces
		Escribir 'Su riesgo es bajo';	
	FinSi
	
	Si (IMC < 22.0) & (edad >= 45) Entonces
		Escribir 'Su riesgo es medio';	
	FinSi
	
	Si (IMC >= 22.0) & (edad < 45) Entonces
		Escribir 'Su riesgo es medio';
	FinSi
	
	Si (IMC >= 22.0) & (edad >= 45) Entonces
		Escribir 'Su riesgo es alto';	
	FinSi
	
FinProceso
